
package mecanismo;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class ImageLoader implements Runnable {
    private final String[] arquivosMenu;
    private final String[] arquivosJogo;
    private final Image[] menuImage;
    private final Image[] jogoImage;
    private final Image[] letraImage;

    public ImageLoader() {
        arquivosMenu = new String[]{"menu_background.png", "iniciar.png", "iniciar_selecionado.png", "tema_background.png" , "icone.png"};
        arquivosJogo = new String[]{"jogo_background.png", "boneco1.png", "boneco2.png", "boneco3.png", "boneco4.png", "boneco5.png", 
            "boneco6.png", "barra.png", "selecionado.png", "bloqueado.png", "hifen.png", "vitoria_icone.png", "morte_icone.png"};
        
        menuImage = new Image[arquivosMenu.length];
        jogoImage = new Image[arquivosJogo.length];
        letraImage = new Image[27];
    }
    
    @Override
    public void run() {
        if (!new File("GUI").exists()) {
            JOptionPane.showMessageDialog(null, "A Pasta \"GUI\" com as imagens não existe!", "Erro!", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        
        int tamanho = arquivosMenu.length;
        try {
            for (int i=0 ; i<tamanho ; i++)
                menuImage[i] = ImageIO.read(new File("GUI\\" + arquivosMenu[i]));
            
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Existem imagens da interface faltando!", "Erro!", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        FrmMain.getInstance().inicializarPainelMenu(menuImage);
        
        tamanho = arquivosJogo.length;
        try {
            for (int i=0 ; i<tamanho ; i++)
                jogoImage[i] = ImageIO.read(new File("GUI\\" + arquivosJogo[i]));
            
            for (int i=0 ; i<26 ; i++)
                letraImage[i] = ImageIO.read( new File("GUI\\Alfabeto\\" + String.valueOf((char)(65 + i)) + ".png") );
            
            letraImage[26] = ImageIO.read( new File("GUI\\Alfabeto\\ç.png"));
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Existem imagens da interface faltando!", "Erro!", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        FrmMain.getInstance().inicializarPainelJogo(jogoImage, letraImage);
    }
}
