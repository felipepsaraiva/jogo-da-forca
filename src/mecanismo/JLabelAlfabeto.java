package mecanismo;

import javax.swing.JLabel;

public class JLabelAlfabeto extends JLabel {

    //Utilizado para saber se a label ja foi clicada alguma vez
    private boolean clicado;

    public JLabelAlfabeto() {
        clicado = false;
    }

    public boolean getClicado() {
        return clicado;
    }

    public void setClicado(boolean valor) {
        clicado = valor;
    }

}
