
package mecanismo;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class PainelTema extends javax.swing.JPanel {

    public PainelTema(Image background) {
        initComponents();
        lblSelecionar.setText("");
        backgroundImage = background;
        modeloLista = new DefaultListModel<>();
    }
    
    @Override
    public void paintComponent(Graphics g) {
        //Desenha o background do tema
        g.drawImage(backgroundImage, 0, 0, null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        listaTemas = new javax.swing.JList();
        lblSelecionar = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(250, 350));
        setLayout(null);

        jScrollPane1.setViewportView(listaTemas);

        add(jScrollPane1);
        jScrollPane1.setBounds(20, 50, 210, 258);

        lblSelecionar.setText("Selecionar");
        lblSelecionar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblSelecionar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSelecionarMouseClicked(evt);
            }
        });
        add(lblSelecionar);
        lblSelecionar.setBounds(159, 322, 91, 17);
    }// </editor-fold>//GEN-END:initComponents

    private void lblSelecionarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSelecionarMouseClicked
        if (!listaTemas.isSelectionEmpty()) {
            //Inicia o jogo com o index selecionado na lista
            FrmMain.getInstance().iniciarJogo(listaTemas.getSelectedIndex());
        }
    }//GEN-LAST:event_lblSelecionarMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblSelecionar;
    private javax.swing.JList listaTemas;
    // End of variables declaration//GEN-END:variables
    
    private Image backgroundImage;
    private DefaultListModel<String> modeloLista;
    
    public void atualizarTemas() {
        //Remove os elementos anteriores
        modeloLista.removeAllElements();
        File pasta = new File("Palavras");
        //Checa se a pasta 'Palavras' existe
        if (!pasta.exists()) {
            JOptionPane.showMessageDialog(FrmMain.getInstance(), "A pasta \"Palavras\" não existe!", "Erro", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        String[] arquivos = pasta.list();
        //Checa se a pasta 'Palavras' não está vazia
        if (arquivos.length == 0) {
            JOptionPane.showMessageDialog(FrmMain.getInstance(), "A pasta \"Palavras\" está vazia!", "Erro", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        //Adiciona a opção 'Aleatório'
        modeloLista.addElement("Aleatório");
        //Adiciona o nome de todos os arquivos existentes dentro da pasta 'Palavras'
        for (int i=0 ; i<arquivos.length ; i++)
            modeloLista.addElement(arquivos[i].replace(".txt", ""));
        //Adiciona a modelo de opções criados
        listaTemas.setModel(modeloLista);
    }
}
