package mecanismo;

import javax.swing.JOptionPane;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;

public class PainelMenu extends javax.swing.JPanel {
    private Image[] menuImagem;
    
    public PainelMenu(Image[] imagens) {
        initComponents();
        lblIniciar.setText("");
        lblAjuda.setText("");
        lblSobre.setText("");
        menuImagem = imagens;
        lblIniciar.setIcon(new ImageIcon(menuImagem[1]));
    }
    
    @Override
    public void paintComponent(Graphics g) {
        //Desenhando Background
        g.drawImage(menuImagem[0], 0, 0, null);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblIniciar = new javax.swing.JLabel();
        lblAjuda = new javax.swing.JLabel();
        lblSobre = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(800, 600));
        setLayout(null);

        lblIniciar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIniciar.setText("Iniciar");
        lblIniciar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblIniciar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblIniciarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblIniciarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblIniciarMouseExited(evt);
            }
        });
        add(lblIniciar);
        lblIniciar.setBounds(290, 400, 261, 155);

        lblAjuda.setText("Ajuda");
        lblAjuda.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblAjuda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblAjudaMouseClicked(evt);
            }
        });
        add(lblAjuda);
        lblAjuda.setBounds(0, 495, 152, 45);

        lblSobre.setText("Sobre");
        lblSobre.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblSobre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSobreMouseClicked(evt);
            }
        });
        add(lblSobre);
        lblSobre.setBounds(0, 428, 146, 43);
    }// </editor-fold>//GEN-END:initComponents

    private void lblSobreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSobreMouseClicked
        String mensagem = "Desenvolvido por:"
                + "\n - \tArthur Andrade"
                + "\n - \tBruna Sousa"
                + "\n - \tFelipe Gabriel"
                + "\n - \tFelipe Saraiva"
                + "\n - \tNeilton Carlos";
        JOptionPane.showMessageDialog(this, mensagem, "Sobre", JOptionPane.PLAIN_MESSAGE, new ImageIcon(menuImagem[4]));
    }//GEN-LAST:event_lblSobreMouseClicked

    private void lblAjudaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAjudaMouseClicked
        String mensagem = "Tente adivinhar a palavra com base no número de letras e no tema."
                + "\nÉ possível clicar nas letras ou digitar a palavra completa!"
                + "\nVocê tem apenas 5 chances de erro ou uma chance de digitar a palavra certa."
                + "\nNo sexto erro, o jogo acaba!";
        JOptionPane.showMessageDialog(this, mensagem, "Ajuda", JOptionPane.QUESTION_MESSAGE, null);
    }//GEN-LAST:event_lblAjudaMouseClicked

    private void lblIniciarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIniciarMouseClicked
        FrmMain.getInstance().escolherTema(menuImagem[3]);
    }//GEN-LAST:event_lblIniciarMouseClicked

    private void lblIniciarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIniciarMouseEntered
        lblIniciar.setIcon(new ImageIcon(menuImagem[2]));
    }//GEN-LAST:event_lblIniciarMouseEntered

    private void lblIniciarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIniciarMouseExited
        lblIniciar.setIcon(new ImageIcon(menuImagem[1]));
    }//GEN-LAST:event_lblIniciarMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblAjuda;
    private javax.swing.JLabel lblIniciar;
    private javax.swing.JLabel lblSobre;
    // End of variables declaration//GEN-END:variables
    
}
