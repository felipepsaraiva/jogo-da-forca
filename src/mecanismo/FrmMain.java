
package mecanismo;

import java.awt.Image;
import java.io.*;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FrmMain extends JFrame {
    //Singleton
    private static FrmMain instance = new FrmMain();
    private static boolean menuCarregado;
    private static boolean jogoCarregado;
    
    //Paineis
    private PainelMenu pnlMenu;
    private PainelTema pnlTema;
    private PainelJogo pnlJogo;
    
    //Atributos do jogo
    private String tema;
    private String palavra;
    private boolean aleatorio;
    
    private FrmMain() {
        //Inicializando o Frame
        super ("Jogo Da Forca");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        
        menuCarregado = false;
        jogoCarregado = false;
        pnlMenu = null;
        pnlTema = null;
        pnlJogo = null;
        tema = null;
        palavra = null;
        aleatorio = false;
    }
    
    //Retorna a instancia única do FrmMain (Padrão Singleton)
    public static FrmMain getInstance() {
        return instance;
    }
    
    //Método Main
    public static void main(String[] args) {
        ImageLoader imageLoader = new ImageLoader();
        new Thread(imageLoader).start();
        FrmMain.getInstance().abrirMenu();
    }
    
    //Checa se o menu foi carregado e adiciona ele no frame, alem de tornar o frame visivel
    public synchronized void abrirMenu() {
        //Espera até que o menu esteja carregado
        while (!menuCarregado) {
            try {
                wait();
            } catch (InterruptedException ex) {}
        }
        limparFrame();
        add(pnlMenu);
        pnlMenu.repaint();
        pack();
        instance.setVisible(true);
    }
    
    //Atualiza os temas e adiciona o painel dos temas na frame
    public void escolherTema(Image background) {
        pnlTema.atualizarTemas();
        limparFrame();
        add(pnlTema);
        pnlTema.repaint();
        pack();
    }
    
    //Inicia o jogo e adiciona o painel do jogo na frame
    public void iniciarJogo(int index) {
        //Checa se as imagens do jogo ja foram carregadas
        if (jogoCarregado) {
            sortearPalavra(index);
            pnlJogo.reiniciar(tema, palavra.toUpperCase());
            limparFrame();
            add(pnlJogo);
            pnlJogo.repaint();
            pack();
        } else
            JOptionPane.showMessageDialog(this, "Aguarde as imagens do jogo serem carregadas!", "Espere!", JOptionPane.INFORMATION_MESSAGE);
    }
    
    //Sorteia a palavra para iniciar o jogo
    public void sortearPalavra(int index) {
        Random random = new Random();
        File arquivo;
        //Define o tema e seleciona o arquivo para pegar a palavra
        String[] listaArquivos = new File("Palavras").list();
        if (index == -1)
            if (aleatorio)
                index = 0;
        
        if (index == 0) {
            index = random.nextInt(listaArquivos.length);
            aleatorio = true;
            tema = listaArquivos[index].replaceAll(".txt", "");
        } else if (index > 0) {
            index--;
            aleatorio = false;
            tema = listaArquivos[index].replaceAll(".txt", "");
        }
        arquivo = new File("Palavras\\" + tema + ".txt");
        
        int numeroLinhas;
        try {
            //Le o numero de linhas que o arquivo possui
            LineNumberReader entrada = new LineNumberReader(new InputStreamReader(new FileInputStream(arquivo), "ISO-8859-1")); //////////////////////////////
            entrada.skip(Long.MAX_VALUE);
            numeroLinhas = entrada.getLineNumber();
            entrada.close();
            
            //Le a linha que foi sorteada
            entrada = new LineNumberReader(new InputStreamReader(new FileInputStream(arquivo), "ISO-8859-1"));
            int linhaSorteada = random.nextInt(numeroLinhas+1);
            for (int i=0 ; i<linhaSorteada ; i++)
                entrada.readLine();
            palavra = entrada.readLine();
            entrada.close();
            
            //Tratamento de Entradas Indevidas
            //NullPointerException: Se a palavra for nula ou for vazia
            if (palavra == null || palavra.trim().equals(""))
                throw new NullPointerException("Existe uma linha vazia no arquivo " + arquivo + "!");
            
            //IllegalArgumentException: Se a palavra possuir menos de 4 letras ou mais de 18 letras
            if (palavra.length() < 4 || palavra.length() > 18)
                throw new IllegalArgumentException("Não são permitidas palavras com " + palavra.length() + " letras!");
            
            //IllegalArgumentException: Se houver números na palavra
            for (int i=0 ; i<10 ; i++)
                if (palavra.contains(String.valueOf(i)))
                    throw new IllegalArgumentException("Não são permitidos números nas palavras!");
            
        } catch (FileNotFoundException ex1) {
            ex1.printStackTrace();
            JOptionPane.showMessageDialog(this, "O Arquivo " + arquivo + " não existe!", "Erro", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erro ao ler o arquivo " + arquivo + "!", "Erro", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        } catch (NullPointerException | IllegalArgumentException ex2) {
            JOptionPane.showMessageDialog(this, ex2.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }
    
    private void limparFrame(){
        if (pnlMenu != null)
            remove(pnlMenu);
        
        if (pnlTema != null)
            remove(pnlTema);
        
        if (pnlJogo != null)
            remove(pnlJogo);
    }
    
    public synchronized void inicializarPainelMenu(Image[] imagens) {
        pnlMenu = new PainelMenu(imagens);
        pnlTema = new PainelTema(imagens[3]);
        this.setIconImage(imagens[4]);
        menuCarregado = true;
        notify();
    }
    
    public synchronized void inicializarPainelJogo(Image[] imagem, Image[] imagemLetra) {
        pnlJogo = new PainelJogo(imagem, imagemLetra);
        jogoCarregado = true;
    }
    
}
