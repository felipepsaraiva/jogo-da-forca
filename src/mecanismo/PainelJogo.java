package mecanismo;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PainelJogo extends JPanel {

    private Image[] imagem;
    private Image[] imagemLetra;

    private JLabelAlfabeto[] alfabeto;
    private JLabel lblTema;
    private JLabel lblVoltar;
    private JTextField txtPalavraFinal;

    private char[] palavra;
    private boolean[] marcador;
    private int erros;

    public PainelJogo(final Image[] imagem, Image[] imagemLetra) {
        //Propriedades do painel
        this.setLayout(null);
        this.setPreferredSize(new Dimension(800, 600));
        
        //Inicializa os vetores de imagens
        this.imagem = imagem;
        this.imagemLetra = imagemLetra;
        
        //Cria, inicializa e adiciona as Labels que ficam posicionadas em cima das letras
        //E adiciona o tratador do evento de clicar, entrar com o mouse e sair com o mouse
        alfabeto = new JLabelAlfabeto[27];
        for (int i = 0; i < 27; i++) {
            alfabeto[i] = new JLabelAlfabeto();
            alfabeto[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
            //Adiciona o Listener para os eventos relacionados ao mouse
            alfabeto[i].addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    //Se a label não foi clicada
                    if (!((JLabelAlfabeto)e.getSource()).getClicado()) {
                        int j = 0;
                        //Checa qual é a posição da label clicada no vetor 'alfabeto'
                        for (j = 0; j < 27; j++) {
                            if (alfabeto[j].equals(((JLabelAlfabeto) e.getSource()))) {
                                break;
                            }
                        }
                        //Pega a letra relativa à label clicada
                        char letra = (char) (j + 65);
                        if (j == 26)
                            letra = 'Ç';
                        
                        //Checa se existe a letra na palavra
                        boolean encontrou = false;
                        for (j = 0; j < palavra.length; j++) {
                            if (letra == palavra[j]) {
                                marcador[j] = true;
                                encontrou = true;
                            }
                        }
                        if (!encontrou) {
                            erros++;
                        }
                        ((JLabelAlfabeto)e.getSource()).setClicado(true);
                        ((JLabelAlfabeto)e.getSource()).setIcon(new ImageIcon(imagem[9]));
                        repaint();
                        checarVitoria();
                    }

                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    if (!((JLabelAlfabeto)e.getSource()).getClicado()) {
                        ((JLabelAlfabeto) e.getSource()).setIcon(new ImageIcon(imagem[8]));
                    }
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    if (!((JLabelAlfabeto)e.getSource()).getClicado()) {
                        ((JLabelAlfabeto)e.getSource()).setIcon(null);
                    }
                }
            });
            if (i <= 10) {
                alfabeto[i].setBounds(363 + (38 * i), 203, 32, 32);
            } else if (i <= 19) {
                alfabeto[i].setBounds(401 + (38 * (i - 11)), 242, 32, 32);
            } else {
                alfabeto[i].setBounds(439 + (38 * (i - 20)), 279, 32, 32);
            }
            add(alfabeto[i]);
        }
        
        //Cria, inicializa e adiciona a label tema
        lblTema = new JLabel();
        lblTema.setFont(new Font("Courier New", Font.PLAIN, 18));
        lblTema.setBounds(295, 35, 460, 45);
        add(lblTema);
        
        //Cria, inicializa e adiciona a label voltar
        lblVoltar = new JLabel();
        lblVoltar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        lblVoltar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FrmMain.getInstance().abrirMenu();
            }
        });
        lblVoltar.setBounds(674, 546, 126, 38);
        add(lblVoltar);
        
        //Cria, inicializa e adiciona o campo de texto da tentativa final
        txtPalavraFinal = new JTextField();
        txtPalavraFinal.setFont(new Font("Courier New", Font.PLAIN, 18));
        txtPalavraFinal.setOpaque(false);
        txtPalavraFinal.setBorder(null);
        txtPalavraFinal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int opcao = JOptionPane.showConfirmDialog(FrmMain.getInstance(), "Você só tem uma chance!\nTem certeza?", "Cuidado!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null);
                if (opcao == JOptionPane.YES_OPTION) {
                    String tentativa = txtPalavraFinal.getText().toUpperCase();
                    tentativa = removerAcentos(tentativa);
                    if (tentativa.equals(new String(palavra))) {
                        for (int i = 0; i < marcador.length; i++) {
                            marcador[i] = true;
                        }
                    } else {
                        erros = 6;
                    }
                    txtPalavraFinal.setText("");
                    repaint();
                    checarVitoria();
                }
            }
        });
        txtPalavraFinal.setBounds(80, 502, 675, 32);
        add(txtPalavraFinal);
    }

    @Override
    public void paintComponent(Graphics g) {
        //Desenhando Background
        g.drawImage(imagem[0], 0, 0, null);

        //Desenhando o boneco
        if (erros > 0) {
            g.drawImage(imagem[erros], 200, 194, null);
        }

        //Desenhando as barras para as letras
        for (int i = 0; i < palavra.length; i++) {
            if (palavra[i] == ' ')
                continue;
            else if (palavra[i] == '-')
                g.drawImage(imagem[10], 84 + (37 * i), 392, null);
            else
                g.drawImage(imagem[7], 84 + (37 * i), 392, null);
        }

        //Desenhando as letras descobertas
        for (int i = 0; i < palavra.length; i++) {
            if (marcador[i]) {
                if (!(palavra[i] == ' ') && !(palavra[i] == '-')) {
                    if (palavra[i] == 'Ç') {
                        g.drawImage(imagemLetra[26], 84 + (37 * i), 387, null);
                    } else {
                        g.drawImage(imagemLetra[((int) palavra[i]) - 65], 84 + (37 * i), 387, null);
                    }
                }
            }
        }
    }

    //Reinicia o painel
    public void reiniciar(String tema, String linha) {
        for (int i = 0; i < 27; i++) {
            alfabeto[i].setClicado(false);
            alfabeto[i].setIcon(null);
        }

        lblTema.setText(tema);
        this.palavra = removerAcentos(linha).toCharArray();
        
        marcador = new boolean[this.palavra.length];
        for (int i = 0; i < this.palavra.length; i++) {
            if (palavra[i] == ' ' || palavra[i] == '-')
                marcador[i] = true;
            else
                marcador[i] = false;
        }

        erros = 0;
        repaint();
    }

    private void checarVitoria() {
        if (erros >= 6) {
            for(int i=0 ; i<marcador.length ; i++)
                marcador[i] = true;
            this.repaint();
            int opcao = JOptionPane.showConfirmDialog(this, "E... Morreu!\nDeseja jogar novamente?", "No céu tem pão?", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, new ImageIcon(imagem[12]));
            if (opcao == JOptionPane.YES_OPTION)
                FrmMain.getInstance().iniciarJogo(-1);
            else
                FrmMain.getInstance().abrirMenu();
                
        } else {
            boolean ganhou = true;
            for (int i = 0; i < marcador.length; i++) {
                if (!marcador[i]) {
                    ganhou = false;
                }
            }
            if (ganhou) {
                int opcao = JOptionPane.showConfirmDialog(this, "Você ganhou!\nDeseja jogar novamente?", "Acertou mizeravi!", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, new ImageIcon(imagem[11]));
                if (opcao == JOptionPane.YES_OPTION)
                    FrmMain.getInstance().iniciarJogo(-1);
                else
                    FrmMain.getInstance().abrirMenu();
            }
        }
    }

    private String removerAcentos(String texto) {
        char[] letras = texto.toCharArray();
        int tamanho = texto.length();
        for (int i = 0; i < tamanho; i++) {
            if (letras[i] == 'Á' || letras[i] == 'Â' || letras[i] == 'Ã') {
                letras[i] = 'A';
            } else if (letras[i] == 'É' || letras[i] == 'Ê') {
                letras[i] = 'E';
            } else if (letras[i] == 'Í' || letras[i] == 'Î') {
                letras[i] = 'I';
            } else if (letras[i] == 'Ó' || letras[i] == 'Õ' || letras[i] == 'Ô') {
                letras[i] = 'O';
            } else if (letras[i] == 'Ú' || letras[i] == 'Û') {
                letras[i] = 'U';
            }
        }
        return new String(letras);
    }

}
